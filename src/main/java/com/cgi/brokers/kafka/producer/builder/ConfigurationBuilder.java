package com.cgi.brokers.kafka.producer.builder;

/**
 * Interface pour les builders.
 *
 * @param <B> Type de l'objet à builder
 * @author damien.cacheux
 */
public interface ConfigurationBuilder<B>
{
    /**
     * Build l'objet.
     *
     * @return L'objet buildé
     * @throws Exception Exception relevée lors du traitement
     */
    B build() throws Exception;
}
