package com.cgi.brokers.kafka.producer;

import java.util.Calendar;
import java.util.Properties;
import java.util.UUID;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.LongSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cgi.brokers.kafka.model.ComplexMessage;
import com.cgi.brokers.kafka.producer.context.CustomKafkaProducerContext;
import com.cgi.brokers.kafka.producer.context.CustomKafkaProducerOptionParser;
import com.cgi.brokers.kafka.producer.properties.PropertiesLoader;
import com.cgi.brokers.kafka.producer.serializer.AvroSerializer;

/**
 * Producteur de messages pour Kafka.
 *
 * @author damien.cacheux
 */
public class CustomKafkaProducer
{
    /**
     * Logger de la classe.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomKafkaProducer.class);

    /**
     * Liste des properties du Producer.
     */
    private static Properties props;

    /**
     * Exécution du producer.
     *
     * @param args Liste des paramètres du producer (args[0] : nombre de messages à générer, args[1] : taille du payload en nombre de caractères)
     */
    public static void main(final String[] args)
    {
        final CustomKafkaProducerContext context = new CustomKafkaProducerOptionParser(args).getCustomKafkaProducerContext();

        CustomKafkaProducer.props = new PropertiesLoader()
                .withFiles("producer.properties")
                .get();

        for (int threadNumber = 0; threadNumber < Integer.parseInt(CustomKafkaProducer.props.getProperty("thread.number")); threadNumber++)
        {
            final int finalThreadNumber = threadNumber;
            new Thread(() -> {
                CustomKafkaProducer.runProducer(context.getNbMessages(), context.getTailleMessages(), finalThreadNumber);
            }).start();
        }
    }

    /**
     * Exécute le producteur et envoie un nombre de messages spécifiques.
     *
     * @param sendMessageCount Nombre de messages à envoyer
     * @param messageSize Taille en nombre de caractères du message.
     */
    private static void runProducer(final Integer sendMessageCount, final Integer messageSize, final Integer theThreadNumber)
    {
        final Producer<Long, ComplexMessage> producer = CustomKafkaProducer.createProducer(theThreadNumber);

        final long time = System.currentTimeMillis();

        try
        {
            int sentMessage = 0;
            for (long index = time; index < time + sendMessageCount; index++)
            {
                final ProducerRecord<Long, ComplexMessage> record = new ProducerRecord<>(CustomKafkaProducer.props.getProperty("producer.topic"), index, ComplexMessage.newBuilder()
                        .setIdentifiant(String.format("%s/%s", System.currentTimeMillis(), UUID.randomUUID().toString()))
                        .setMessage(RandomStringUtils.randomAlphanumeric(messageSize))
                        .setCurrentDate(Calendar.getInstance().getTime().toString())
                        .build());

                final RecordMetadata metadata = producer.send(record).get();

                final long elapsedTime = System.currentTimeMillis() - time;

                if (CustomKafkaProducer.LOGGER.isDebugEnabled())
                {
                    LOGGER.debug(String.format("sent record(key=%s value=%s) meta(partition=%d, offset=%d) time=%d\n", record.key(), record.value(), metadata.partition(), metadata.offset(), elapsedTime));
                }

                sentMessage++;

                final float pourcentage = ((float) sentMessage / (float) sendMessageCount) * 100;
                if ((pourcentage % 10) == 0)
                {
                    LOGGER.info(String.format("[Thread-%s] Pourcentage messages envoyés : %s /100", theThreadNumber, pourcentage));
                }
            }
            final long finalTime = System.currentTimeMillis();

            final String tempsPasse = "" + (finalTime - time) / 1000 + " secondes " + (finalTime - time) % 1000 + " ms";
            LOGGER.info(String.format("[Thread-%s] Temps passé pour traiter %s messages : %s", theThreadNumber, sendMessageCount, tempsPasse));
        } catch (final Exception theException)
        {
            System.out.printf("Exception relevée %s", theException.getMessage());
        } finally
        {
            producer.flush();
            producer.close();
        }
    }

    /**
     * Initialise le producteur de messages pour Kafka.
     *
     * @return Le producteur initialisé
     */
    private static Producer<Long, ComplexMessage> createProducer(final int theThreadNumber)
    {
        final Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, CustomKafkaProducer.props.getProperty("bootstrap.servers"));
        props.put(ProducerConfig.CLIENT_ID_CONFIG, String.format("%s-%s", CustomKafkaProducer.props.getProperty("producer.client.id"), theThreadNumber));
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, AvroSerializer.class);

        return new KafkaProducer<>(props);
    }
}
